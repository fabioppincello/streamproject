package com.pincello.stream.processor;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.pincello.stream.Stream;
import com.pincello.stream.impl.PincelloStream;

public class StreamProcessorTest {
	
	@Test
	public void test1() throws Exception {
		String string = "aAbBABacafe";
		Stream stream = new PincelloStream(string);
		char firstChar = StreamProcessor.firstChar(stream);
		assertTrue(firstChar == 'e');
	}
	
	@Test
	public void test2() throws Exception {
		String string = "aAbBABacafefi";
		Stream stream = new PincelloStream(string);
		char firstChar = StreamProcessor.firstChar(stream);
		assertTrue(firstChar == 'e');
	}
	
	@Test(expected = Exception.class)
	public void test3() throws Exception {
		String string = "xOpTAxAbA";
		Stream stream = new PincelloStream(string);
		StreamProcessor.firstChar(stream);
	}
	
	@Test(expected = Exception.class)
	public void test4() throws Exception {
		String string = "aAbBABacafeaAbBABacafe";
		Stream stream = new PincelloStream(string);
		StreamProcessor.firstChar(stream);
	}
	
	@Test(expected = Exception.class)
	public void test5() throws Exception {
		String string = "kbAleaFXbefa";
		Stream stream = new PincelloStream(string);
		StreamProcessor.firstChar(stream);
	}
}
