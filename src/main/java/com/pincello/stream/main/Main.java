package com.pincello.stream.main;

import org.apache.commons.lang3.RandomStringUtils;

import com.pincello.stream.Stream;
import com.pincello.stream.impl.PincelloStream;
import com.pincello.stream.processor.StreamProcessor;

/**
 * 
 * @author Fabio Pires Pincello
 * @version 1.0
 * @since 06/04/2019
 *
 */
public class Main {
	
	private static final int STRING_LENGTH = 50;
	private static final boolean USE_LETTERS = true;
	private static final boolean USE_NUMBERS = false;

	/**
	 * M�todo utilizado para execu��o da aplica��o.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		String value = null;
		if(args != null && args.length > 0) {
			value = args[0];
		}
		new Main().run(value);
	}
	
	/**
	 * Execu��o do processamento de uma String.
	 * Caso n�o tenha sido passada como par�metro, uma nova String
	 * � gerada aleatoriamente.
	 * 
	 * @param value
	 */
	private void run(String value) {
		
		if(value == null) {
			value = RandomStringUtils.random(STRING_LENGTH, USE_LETTERS, USE_NUMBERS);
		}
		
		System.out.print(String.format("Processando a String: \"%s\"...\n", value));
		Stream stream = new PincelloStream(value);

		char firstChar;
		try {
			firstChar = StreamProcessor.firstChar(stream);
			System.out.println("Caractere encontrado: " + firstChar);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}
