package com.pincello.stream.impl;

import com.pincello.stream.Stream;

/**
 * Implementação da Stream
 * 
 * @author Fabio Pires Pincello
 * @version 1.0
 * @since 06/04/2019
 *
 */
public class PincelloStream implements Stream {
	
	private String value;
	private int position;
	
	/**
	 * 
	 * @param value
	 */
	public PincelloStream(String value) {
		this.value = value;
		this.position = 0;
	}

	/**
	 * @see Stream#getNext()
	 */
	public char getNext() {
		return value.charAt(position++);
	}

	/**
	 * @see Stream#hasNext()
	 */
	public boolean hasNext() {
		return value.length() > position;
	}
	
}
