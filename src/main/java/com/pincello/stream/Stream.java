package com.pincello.stream;

/**
 * 
 * @author Fabio Pires Pincello
 * @version 1.0
 * @since 06/04/2019
 *
 */
public interface Stream {
	
	/**
	 * Retorna o pr�ximo caractere a ser processado na stream.
	 * 
	 * @return
	 */
	public char getNext();

	/**
	 * Informa se a stream ainda cont�m caracteres a processar.
	 * 
	 * @return
	 */
	public boolean hasNext();

}
