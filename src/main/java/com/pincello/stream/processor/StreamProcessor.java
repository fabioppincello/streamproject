package com.pincello.stream.processor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import com.pincello.stream.Stream;

/**
 * 
 * @author Fabio Pires Pincello
 * @version 1.0
 * @since 06/04/2019
 *
 */
public class StreamProcessor {
	
	private enum State {
		ESTADO_INICIAL,
		VOGAL_ENCONTRADA,
		CONSOANTE_APOS_VOGAL_ENCONTRADA;
	}

	private static ArrayList<Character> vogais = new ArrayList<Character>(
			Arrays.asList('a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U'));

	/**
	 * De acordo com a Stream recebida como argumento, esse m�todo busca pelo
	 * primeiro caractere vogal ap�s uma consoante, onde a mesma � antecessora
	 * a uma vogal e que n�o se repita no resto da Stream.
	 * Caso o caractere seja encontrado, o m�tod retorna o caractere.
	 * Caso contr�rio, o m�todo lan�a uma exce��o informando que o caractere
	 * n�o foi encontrado.
	 * 
	 * @param input
	 * @return
	 * @throws Exception
	 */
	public static char firstChar(Stream input) throws Exception {
		
		State state = State.ESTADO_INICIAL;
		
		List<Character> vogaisUsadas = new ArrayList<Character>();
		List<Character> candidatasASucesso = new ArrayList<Character>();

		while (input.hasNext()) {
			char character = input.getNext();
			if(vogais.contains(character)) {
				if(!vogaisUsadas.contains(character)) {
					vogaisUsadas.add(character);
					if(state.equals(State.CONSOANTE_APOS_VOGAL_ENCONTRADA)) {
						candidatasASucesso.add(character);
					}
				} else if(candidatasASucesso.contains(character)) {
					Iterator<Character> iterator = candidatasASucesso.iterator();
					while(iterator.hasNext()) {
						if(iterator.next().equals(character)) {
							iterator.remove();
							break;
						}
					}
				}
				state = State.VOGAL_ENCONTRADA;
			} else if(state.equals(State.VOGAL_ENCONTRADA)) {
				state = State.CONSOANTE_APOS_VOGAL_ENCONTRADA;
			} else {
				state = State.ESTADO_INICIAL;
			}
		}
		
		if(candidatasASucesso.isEmpty()) {
			throw new Exception("Caractere n�o encontrado");
		}
		
		return candidatasASucesso.get(0);
	}

}
