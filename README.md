##4) O que é Deadlock? Detalhe um pouco sobre o caso e como você poderia resolver isso.

O que se espera como resposta - Dicas e direcionamentos:

* Detalhe sua linha de raciocínio;
* Elabore um plano de entendimento, por exemplo, lista, de forma a elencar os passos;
* Não copie conteúdo da internet, responda com suas palavas.
---
O deadlock é uma situação de travamento, ou impedimento, que ocorre em um conjunto de processos quando todos os processos desse conjunto aguardam por um um evento que dependem de um outro processo desse mesmo conjunto.

Exemplos de casos possíveis de deadlock:

1. Os processos **A** e **B** necessitam de acesso exclusivo aos recursos **1** e **2**, que são *não-preemptíveis*, ou seja, uma vez que são concedidos a um processo, só podem ser liberados pelo processo que o retém.
    Durante a execução dos dois processos, os passos abaixo seguem de forma linear:

    * O processo **A** retém o acesso ao recurso **1** e só irá liberá-lo após seu processamento que depende do recurso **2**.
    * O processo **B** retém o acesso ao recurso **2** e só irá liberá-lo após seu processamento que depende do recurso **1**.
    * O processo **A** solicita acesso ao recurso **2**.
    * O processo **B** nega a solicitação pois está usando o recurso.
    * O processo **B** solicita acesso ao recurso **1**.
    * O processo **A** nega a solicitação pois está usando o recurso.

    Nesse momento esses processos estão na situação de deadlock, pois ambos dependem de um evento do outro processo para prosseguir com seu processamento. Essa situação permanece inerte a menos que haja uma interferência externa (humana, que seja...). Esse mesmo exemplo pode ser reproduzido com mais de dois processos, em uma cadeia circular, imaginando uma mesma situação com os processos **A**, **B** e **C** e os recursos **1**, **2** e **3**, em que **A** retém **1** e espera por **2**, **B** retém **2** e espera por **3** e **C** retém **3** e espera por **1**.

2. O famoso problema do *Jantar dos Filósofos*, em que há a concorrência entre os garfos na mesa, e dependendo do algoritmo usado pelos filósofos para obter os dois garfos e comer, o sistema pode entrar em deadlock.

Para o exemplo **(1)**, uma das formas de evitar isso, se possível, seria evitar a não-preempção dos recursos, permitindo a retirada de um recurso de um determinado processo quando outro processo necessitasse utilizá-lo.

Para o exemplo **(2)**, uma forma de evitar o deadlock é garantir que cada filósofo, ao tentar comer, nunca bloqueie um garfo antes de ter acesso ao outro, ou seja, somente poderá bloquear os dois garfos que ele tem acesso quando os dois garfos estiverem disponíveis no mesmo instante.

---
##5) Uma das grandes inclusões no java 8 foi a API Stream. Com ela podemos fazer diversas operações de loop, filtros, maps, etc. Porém, existe uma variação bem interessante do Stream que é ParallelStreams. Descreva com suas palavras qual é a diferença entre os dois e quando devemos utilizar cada um deles.

O que se espera como resposta - Dicas e direcionamentos:

* Detalhe sua linha de raciocínio;
* Elabore um plano de entendimento, por exemplo, lista, de forma a elencar os passos;
* Não copie conteúdo da internet, responda com suas palavas.
---
A diferença entre os dois é que o Parallel Stream aproveita a arquitetura de múltiplos núcleos do computador.
Com a Parallel Stream, a Stream é particionada em substreams, que são processadas em paralelo e depois seus resultados são combinados. 
A Parallel Stream pode trazer um ganho de performance no processamento de uma coleção grande, mas não deve ser usada nas iterações onde a computação de um determinado elemento depende ou impacta a de outro elemento da coleção. Nesse caso, o paralelismo perde sua performance, gerando muito overhead no gerenciamento das substreams. Outra desvantagem no uso das Parallel Streams é quando ela é utilizada com listas que não são baseadas em índice, como as LinkedList, por exemplo. Para esses casos, deve-se preferir a utilização das Stream sequenciais mesmo.